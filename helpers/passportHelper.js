var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// Local Strategy for sign-up
passport.use('local-signup', new LocalStrategy({
        usernameField: 'nick',
        passwordField: 'password',
        passReqToCallback: true,
    },
    function (req, nick, password, done) {
        req.app.models.user.findOne({nick: nick}, function (err, user) {
            if (err) {
                return done(err);
            }
            if (user) {
                return done(null, false, {message: 'Email address already taken, please choose an another one.'});
            }
            req.app.models.user.create(req.body)
                .then(function (user) {
                    return done(null, user);
                })
                .catch(function (err) {
                    return done(null, false, {message: err.details});
                })
        });
    }
));

// Stratégia
passport.use('local', new LocalStrategy({
        usernameField: 'nick',
        passwordField: 'password',
        passReqToCallback: true,
    },
    function (req, nick, password, done) {
        req.app.models.user.findOne({nick: nick}, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user || !user.validPassword(password)) {
                return done(null, false, {message: 'Wrong username or password'});
            }
            return done(null, user);
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (obj, done) {
    done(null, obj);
});

module.exports = passport;