var bcrypt = require('bcryptjs');

module.exports = {
    identity: 'user',
    connection: 'default',
    attributes: {
        email: {
            type: 'string',
            required: true,
            unique: true,
        },
        password: {
            type: 'string',
            required: true,
        },
        nick: {
            type: 'string',
            required: true,
            unique: true,
        },
        hashtags: {
            type: 'string',
        },
        avatar: {
            type: 'string',
        },
        address: {
            type: 'string',
        },
        friends: {
            type: 'array',
        },
        share: {
            collection: 'share',
            via: 'owner',
        },
        validPassword: function (password) {
            return bcrypt.compareSync(password, this.password);
        },
    },
    beforeCreate: function(values, next) {
        bcrypt.hash(values.password, 10, function(err, hash) {
            if (err) {
                return next(err);
            }
            values.password = hash;
            next();
        });
    }
};