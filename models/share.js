module.exports = {
    identity: 'share',
    connection: 'default',
    attributes: {
        url: {
            type: 'string',
            required: true,
            unique: true,
        },
        to: {
            model: 'user'
        },
        owner: {
            model: 'user'
        }
    }
};