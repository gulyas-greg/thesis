var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('connect-flash');
var waterline = require('waterline');
var hbs = require('hbs');

var indexRouter = require('./controllers/index');
var loginRouter = require('./controllers/login');
var profileRouter = require('./controllers/profile');
var searchRouter = require('./controllers/search');
var shareRouter = require('./controllers/share');

var waterlineConfig = require('./config/waterline');

var userCollection = require('./models/user');
var shareCollection = require('./models/share');

var passport = require('./helpers/passportHelper');

//----

hbs.registerHelper('sizeof', function (array) {
    return array.length;
});

// Middleware segédfüggvény
function setLocalsForLayout() {
    return function (req, res, next) {
        res.locals.loggedIn = req.isAuthenticated();
        res.locals.user = req.user;
        next();
    }
}
function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}
function andRestrictTo(role) {
    return function (req, res, next) {
        if (req.user.role == role) {
            next();
        } else {
            next(new Error('Unauthorized'));
        }
    }
}

//----

var app = express();

//config
app.set('views', './views');
app.set('view engine', 'hbs');

app.use(express.static('public'));

app.use(bodyParser.urlencoded({extended: false}));
app.use(session({
    cookie: {maxAge: 600000},
    secret: 'titkos szoveg',
    resave: false,
    saveUninitialized: false,
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use(setLocalsForLayout());

//endpoint
app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/profile', ensureAuthenticated, profileRouter);
app.use('/search', ensureAuthenticated, searchRouter);
app.use('/share', ensureAuthenticated, shareRouter);

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

//-----

var orm = new waterline();
orm.loadCollection(waterline.Collection.extend(userCollection));
orm.loadCollection(waterline.Collection.extend(shareCollection));

// ORM indítása
orm.initialize(waterlineConfig, function (err, models) {
    if (err) throw err;

    app.models = models.collections;
    app.connections = models.connections;

    // Start Server
    var port = process.env.PORT || 80;
    app.listen(port, function () {
        console.log('Server is started.');
    });

    console.log("ORM is started.");
});

