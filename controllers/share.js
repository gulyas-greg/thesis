var express = require('express');

var router = express.Router();

router.get('/', function (req, res) {
    req.app.models.share.find({owner: req.user.id}).then(function (result) {
        res.render('share/index', {
            layout: 'layout',
            shares: result,
        });
    })
});

router.post('/', function (req, res) {
    if (req.body.delete) {
        req.app.models.share.destroy({id: req.body.id}).then(function (result) {
            res.redirect('back');
        })
    }
});

module.exports = router;