var express = require('express');
var passport = require('passport');

var router = express.Router();


//Login

router.get('/', function (req, res) {
    res.render('login/index', {
        layout: 'login/reg',
        errorMessages: req.flash('error')
    });
});

router.post('/', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true,
    badRequestMessage: 'Error...'
}));

//Registration

router.get('/reg', function (req, res) {
    res.render('login/registration', {
        layout: 'login/reg',
        errorMessages: req.flash('error')
    });
});

router.post('/reg', passport.authenticate('local-signup', {
    successRedirect: '/',
    failureRedirect: '/login/reg',
    failureFlash: true,
    badRequestMessage: 'Error...'
}));

module.exports = router;