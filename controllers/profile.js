var express = require('express');
var passport = require('passport');

var router = express.Router();

function contains(a, obj) {
    if (a) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] == obj) {
                return true;
            }
        }
    }
    return false;
}

function fillFriends(allUser, you) {
    var friends = [];
    var size;
    if (allUser) {
        for (var i = 0; i < allUser.length; i++) {
            if (contains(you.friends, allUser[i].id))
                friends.push(allUser[i]);
        }
    }
    return friends;
}

function getFollowers(result, yourId) {
    if (result) {
        var followers = [];
        for (var i = 0; i < result.length; i++) {
            if (contains(result[i].friends, yourId))
                followers.push(result[i])
        }
    }
    return followers;
}

router.get('/', function (req, res) {
    req.app.models.user.find().exec(function (err, result) {
        res.render('profile', {
            layout: 'layout',
            messages: req.flash('info'),
            following: fillFriends(result, req.user),
            followers: getFollowers(result, req.user.id)
        });
    });
});

router.get('/settings', function (req, res) {
    res.render('profile/settings', {layout: 'layout'});
});

router.post('/settings', function (req, res) {
    if (req.body.share) {
        req.flash('info', "Sorry it's not supported yet");
        res.redirect('/profile');
    } else {
        req.app.models.user.update({nick: req.user.nick}, {
            avatar: req.body.avatar,
            hashtags: req.body.hashtags,
            address: req.body.address
        })
            .exec(function (err, newUser) {
                req.session.passport.user = newUser[0];
                req.flash('info', 'Your settings successfully changed');
                res.redirect('/profile');
            });
    }
});


module.exports = router;