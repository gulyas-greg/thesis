var express = require('express');

var router = express.Router();

function remove(array, item) {
    var index = array.indexOf(item);
    array.splice(index, 1);
    return array;
}

function ifContainsDeleteEitherCreate(req) {
    if (typeof req.user.friends == 'undefined') {
        var newFriends = [];
        newFriends.push(req.body.id);
        req.user.friends = newFriends;
        return req.user.friends;
    }
    if (contains(req.user.friends, req.body.id)) {
        req.user.friends = remove(req.user.friends, req.body.id);
    } else {
        req.user.friends.push(req.body.id);
    }
    return req.user.friends;
}

function contains(a, obj) {
    if (a) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] == obj) {
                return true;
            }
        }
    }
    return false;
}

function filterYourselfAndSetFriend(array, you) {
    var passedTest = [];
    for (var i = 0; i < array.length; i++) {
        if (contains(you.friends, array[i].id))
            array[i]['yourFriend'] = true;

        if (array[i].id !== you.id)
            passedTest.push(array[i]);

    }
    return passedTest;
}

function setFriends(array, you) {
    for (var i = 0; i < array.length; i++) {
        if (contains(you.friends, array[i].id)) {
            array[i]['yourFriend'] = true;
        }
    }
    return array;
}

function renderSite(req, res) {
    req.app.models.user.find().then(function (result) {
        res.render('search/index', {
            layout: 'layout',
            errorMessages: req.flash('alert'),
            messages: req.flash('info'),
            searchResult: filterYourselfAndSetFriend(result, req.user)
        });
    })

}

router.get('/', function (req, res) {
    renderSite(req, res);
});

router.get('/nick/', function (req, res) {
    renderSite(req, res);
});

router.get('/hash/', function (req, res) {
    renderSite(req, res);
});

router.get('/nick/:nick', function (req, res) {
    var crit = req.params.nick;
    req.app.models.user.find().where({nick: {contains: crit}}).then(function (users) {
        res.render('search/index', {
            layout: 'layout',
            errorMessages: req.flash('alert'),
            messages: req.flash('info'),
            searchResult: filterYourselfAndSetFriend(users, req.user)
        });
    })
});

router.get('/hash/:hash', function (req, res) {
    var crit = req.params.hash;
    req.app.models.user.find().where({hashtags: {contains: crit}}).then(function (users) {
        res.render('search/index', {
            layout: 'layout',
            errorMessages: req.flash('alert'),
            messages: req.flash('info'),
            searchResult: setFriends(users, req.user),
        });
    })
});

router.post('/', function (req, res) {
    posting(req, res);
});

router.post('/nick/', function (req, res) {
    posting(req, res);
});

router.post('/hash/', function (req, res) {
    posting(req, res);
});

router.post('/hash/:hash', function (req, res) {
    posting(req, res);
});

router.post('/nick/:nick', function (req, res) {
    posting(req, res);
});

function posting(req, res) {
    if (req.body.follow) {
        var newFriends = ifContainsDeleteEitherCreate(req);
        req.app.models.user.update({nick: req.user.nick}, {friends: newFriends}).
            exec(function (err, newUser) {
                req.flash('info', 'Successfully followed/unfollowed!');
                res.redirect('back');
            })
    } else if (req.body.share) {
        req.app.models.share.create({
            url: 'sharing' + req.body.id,
            to: req.body.id,
            owner: req.user.id
        }).exec(function (err, result) {
            if (err)
                req.flash('alert', 'You already created a share with him/her!');
            req.flash('info', 'Successfully made a share with him/her');
            res.redirect('back');
        })
    }
}

module.exports = router;