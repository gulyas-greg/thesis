# My thesis work
- [Basic description of the project](https://www.github.com/Gui-greg/thesis#description)
- [Used technologies](https://www.github.com/Gui-greg/thesis#technologies)
- [Models](https://www.github.com/Gui-greg/thesis#models)
- [User interface](https://www.github.com/Gui-greg/thesis#user-interface)
- [Testing](https://www.github.com/Gui-greg/thesis#tests)

##Description
This project is made as a homework but later it will be used as my thesis work.
The basic thing what can be made on this site at this moment is to get followers and be followed
by others. It's a simple thing, you can search by hashtags or by name. After that you can follow 
them or create a share link. The share link has a one-to-one connection between the you(the user) 
and the one who you'd like to follow. It has a unique id. Later this id can be accessed by url
and you can share your files between you two. You can also set your address, hashtags and also 
your avatar to find yourself easier.

##Technologies
This project mainly written in javascript/jquery. It uses a bootstrap what was made by
[Creative-Tim](http://creative-tim.com) ( thank you it helped a lot :) ). The git repository
 doesn't contains the node modules so here is what was used for this.
 
 * express
 * express-session
 * hbs
 * body-parser
 * connect-flash
 * bcryptjs
 * passport
 * passport-local
 * sails-disk
 * sails-memory
 * waterline
 * chai
 * mocha

##Models
![Database model](https://github.com/Gui-greg/thesis/blob/master/documentation/model.png)

Here you can see a little picture about the model.
Short version: There could be many user. Users have a field friends. This field
is an array with id's of other users. This connection is used for the followers and following
on the profile. It has a hashtags field what is used for search. 
**One user can have mutlipleshares(share links) but one share can only owned by one user (One-to-many connection).**
**Users have two unique field: nick and email, Share's unique field is URL**

##User interface
![Base user interface](https://github.com/Gui-greg/thesis/blob/master/documentation/ui.png)

Short things about user interface. In the top left hand corner there is the logo if you click on that
you'll be redirected to the homepage. If you are not logged in the right hand corner has a Login/Register button
after you click on that you can login or register. If you logged in you have a Search field, Welcome and your name 
button if and a logout. If you click on the last one you'll be logged out. WOW. If you click on the Welcone and your name
button you'll be on your profile. The search field ... guess what? if you write a name or a hashtag it will search 
for that and display it for you. WOOOW!!! Other things if not simple enough you wont use this site and you'll never read
this to know what button does what. So build simple site, dont make documentation!

##Tests

There are two main testing method what was used for this project.

* The first one was the mocha/chai based testing. This tests the models that a model can be 
initalised (created) and then it can be found in the database and it can be deleted.
This test just cover the database part and the ORM/waterline/sails part that works.

* The other tests are the "e2e" functional tests based on the gui. Thes tests are written
in zombie js. The tests are as simple as it could. Just try the page basic functions and 
check that these are working or not. Example: create a user and then try to log in and out.

##Development and usage
I used WebStorm as the IDE of the project and it was written on both Windows and fedora.
If you want to continue the project just simply download install the js frameworks what 
was described in the Technologies part. (npm install package --save) then start the server.js
file. Of course you'll need nodejs to be installed and also a web browser.

For the development or usage you'll need a PC that has about 2GB ram, some core i processor or 
the equivalent amd. Graphics are just a integrated one enough for that.



##Feature list 
**This list is just a reminder for me what are done and what need to be done**

- Registration/Login works well with encripted password
- Nick and email unique it gives error if any of these fails
- landing page, reg, login page works
- session works
- profile/share/search only accessable if you autheticated
- profile page should display users dinamically
- pfofile page can be edited and the followers/following ppl are displayed dinamically
- share page implemented it can be deleted
- search page can be accessed and it can display your search

Feature to be implemented:
- validation on server side
- tests
