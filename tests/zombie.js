process.env.NODE_ENV = 'test';
var app = require('../server');
// use zombie.js as headless browser
var Browser = require('zombie');

describe('contact page', function () {
    before(function () {
        this.server = http.createServer(app).listen(8080);
        // initialize the browser using the same port as the test application
        this.browser = new Browser({site: 'http://localhost:8080'});
    });

    // load the contact page
    before(function (done) {
        this.browser.visit('/', done);
    });

    // load Node.js assertion module
    var assert = require('assert');

    describe('login/registration', function () {
        // ...

        it('should show the login page', function () {
            assert.ok(this.browser.success);
            assert.equal(this.browser.text(''), 'Contact');
            assert.equal(this.browser.text('label'), 'Nick');
            assert.equal(this.browser.text('label'), 'Password');
        });

        it('should refuse the empty login', function (done) {
            browser.pressButton('Login', function (error) {
                if (error) return done(error);
                assert.ok(this.browser.success);
                assert.equal(this.browser.text('h1'), 'Contact');
                assert.equal(this.browser.text('label'), 'Nick');
                assert.equal(this.browser.text('label'), 'Password');
                done();
            });
        });

        it('should navigate us to the registration page', function () {
            browser.pressButton('Registration', function (error) {
                assert.ok(this.browser.success);
                assert.true(this.browser.url("/login/reg")); //or something like that
                assert.equal(this.browser.text('h1'), 'Contact');
                assert.equal(this.browser.text('label'), 'Email');
                assert.equal(this.browser.text('label'), 'Nick name');
                assert.equal(this.browser.text('label'), 'Password');
                assert.equal(this.browser.text('label'), 'Password again');
            });

            it('should refuse the empty registration', function (done) {
                browser.pressButton('Login', function (error) {
                    if (error) return done(error);
                    assert.ok(this.browser.success);
                    assert.equal(this.browser.text('h1'), 'Contact');
                    assert.equal(this.browser.text('label'), 'Nick');
                    assert.equal(this.browser.text('label'), 'Password');
                    done();
                });
            });

            after(function (done) {
                this.server.close(done);
            });

        });
    });
});