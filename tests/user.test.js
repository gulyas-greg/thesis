var expect = require("chai").expect;
var bcrypt = require("bcryptjs");

var Waterline = require('waterline');
var waterlineConfig = require('../config/waterline');
var userCollection = require('../models/user');
var shareCollection = require('../models/share');

var User;
var Share;

before(function (done) {
    var orm = new Waterline();

    orm.loadCollection(Waterline.Collection.extend(userCollection));
    orm.loadCollection(Waterline.Collection.extend(shareCollection));
    waterlineConfig.connections.default.adapter = 'memory';

    orm.initialize(waterlineConfig, function (err, models) {
        if (err) throw err;
        User = models.collections.user;
        Share = models.collections.share;
        done();
    });
});

describe('UserModel', function () {

    function getUserData() {
        return {
            email: 'abcdef@abc.com',
            nick: 'peti',
            password: 'password',
            hashtags: '#hash',
            avatar: 'alma',
            address: 'otthon',
        };
    }

    function getShareData() {
        return {
            url: 'http://almafa.com/share',
            owner: 2,
            to: 1
        }
    }

    function checkUserData(user) {
        expect(user.email).to.equal('abcdef@abc.com');
        expect(user.nick).to.equal('peti');
        expect(bcrypt.compareSync('password', user.password)).to.be.true;
        expect(user.hashtags).to.equal('#hash');
        expect(user.avatar).to.equal('alma');
        expect(user.address).to.equal('otthon');
    }

    function checkShareData(share) {
        expect(share.url).to.equal('http://almafa.com/share');
        expect(share.to).to.equal(1);
        expect(share.owner).to.equal(2);
    }

    beforeEach(function (done) {
        User.destroy({}, function (err) {
            done();
        });
    });

    beforeEach(function (done) {
        Share.destroy({}, function (err) {
            done();
        });
    });

    it('should able to create a share', function () {
        return Share.create(getShareData())
            .then(function (share) {
                checkShareData(share);
            })
    });

    it('should be able to find the share', function () {
        return Share.create(getShareData())
            .then(function (share) {
                return Share.findOne({url: share.url});
            })
            .then(function (share) {
                checkShareData(share);
            });
    });

    it('should be able to create a user', function () {
        return User.create(getUserData())
            .then(function (user) {
                checkUserData(user);
            });
    });

    it('should be able to find a user', function () {
        return User.create(getUserData())
            .then(function (user) {
                return User.findOneByNick(user.nick);
            })
            .then(function (user) {
                checkUserData(user);
            });
    });
});